<h1>Case Pilar</h1>
Desenvolvido por João Vitor Specht Kogut


<h3>RESUMO</h3>
Este app apresenta uma API com duas chamadas disponiveis:

<h4>/sort</h4>Realiza a organização das palavras passadas de maneira alfabetica, crescente ou decrescente
<br>metodo: POST<br>Tipo: app/JSON<br> 
<strong>Campos obrigatorios: </strong> "words", lista das palavras a ordenar<br>
<strong>Campos opcionais: </strong> "order", define a ordenação, por padrão crescente, pode ser "desc" para decrescente ou "asc" para crescente<br>
<strong>Ex de requisição: </strong> {"words": ["batman", "robin", "coringa"], "order": "asc"}<br>
<strong>Ex de resposta: </strong> ["batman", "coringa", "robin"]


<h4>/vowel_count</h4>Realiza a contagem de vogais por palavra passada
<br>metodo: POST<br>Tipo: app/JSON<br> 
<strong>Campos obrigatorios: </strong> "words", lista das palavras a processar<br>
<strong>Ex de requisição: </strong> {"words": ["batman", "robin", "coringa"]}<br>
<strong>Ex de resposta: </strong> {"batman": 2, "robin": 2, "coringa": 3}


<br>
<h3>DEPLOY LOCAL</h3>
Para realizar o delpoy local é necessario primeiramente realizar o pull do App. Com seu PowerShell/Terminal/GitBash va ao local onde deseje copiar o projeto e execute o seguinte comando:<br>
<code>git clone https://gitlab.com/vitor_kogut/teste_Pilar.git</code><br>
Então basta executar o arquivo <code>main.py</code>, pode ser executado tanto pelo terminal quanto pela GUI. Após esse procedimento a API estara disponivel em http://localhost:5000



<br>
<h3>TESTE AUTOMATIZADO</h3>
Esta configurado um teste automatizado para checar as funções da API, o teste roda com cada merge realizado a master do projeto.<br>Ele testa as respostas para conferir se são como o esperado e estão funcionando.


<br>
<h3>DEPLOY NA AWS</h3>
Atualmente o App esta rodando na AWS em uma maquina EC2. Para utilizar a API use o endereço <code>ec2-54-225-25-139.compute-1.amazonaws.com:5000/</code> :)
