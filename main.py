##############################################
#                                            #
#       Feito por Joao Vitor Specht Kogut    #
#       Teste para Pilar                     #
#                                            #
##############################################

####### IMPORTS #########
from flask import Flask, request, jsonify



####### CONFIGS #########
app = Flask(__name__)



####### ROUTES ##########
@app.route("/vowel_count", methods=['POST'])
def vowel_count():
    payload = {}

    if request.method != 'POST': # caso nao seja uma chamada POST
        return {"error": "Method Not Allowed"}, 405
    
    if not request.is_json: # caso nao seja um json
        return {"error": "Esta chamada espera um payload tipo application/json!"}, 400
    else:
        payload = request.get_json() # pega dados da request

    if not "words" in payload: # caso a chamada nao possua os dados necessarios
        return {"error": "Chamada sem os dados necessarios!"}, 400

    array_palavras = payload["words"]
    vogais = ["a","e","i","o","u"]
    retorno = {}

    for palavra in array_palavras:
        vogais_da_palavra = 0
        for letra in palavra:
            if letra in vogais:
                vogais_da_palavra = vogais_da_palavra + 1
        
        retorno[palavra] = vogais_da_palavra

    return retorno, 200


@app.route("/sort", methods=['POST'])
def sort():
    payload = {}
    ordenacao = "asc" # asc por padrao, caso exista o campo na chamada sera sobreescrito

    if request.method != 'POST': # caso nao seja uma chamada POST
        return {"error": "Method Not Allowed"}, 405
    
    if not request.is_json: # caso nao seja um json
        return {"error": "Esta chamada espera um payload tipo application/json!"}, 400
    else:
        payload = request.get_json() # pega dados da request

    if not "words" in payload: # caso a chamada nao possua os dados necessarios
        return {"error": "Chamada sem os dados necessarios!"}, 400

    if "order" in payload: # caso seja passada uma ordenacao de preferencia
        ordenacao = payload["order"]
        if ordenacao != "desc" and ordenacao != "asc": # caso a ordenacao passada nao seja suportada
            return {"error": "Metodo de ordenacao nao suportado!"}, 400

    if ordenacao == "asc":
        return jsonify( sorted(payload["words"]) ), 200
    else:
        return jsonify( sorted(payload["words"], reverse=True) ), 200
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
