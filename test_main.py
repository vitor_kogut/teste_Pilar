######## IMPORTS ########
import unittest
import requests


class TestCalls(unittest.TestCase):
    def test_vowel_count(self):
        dados = {"words":["a","b"]}
        request_test = requests.post("http://localhost:5000/vowel_count", json=dados) # Realiza a request
        resposta = request_test.json() # pega os dados da resposta
        self.assertEqual(request_test.ok, True) # testa a request
        self.assertEqual(resposta['a'], 1) # confere se a é uma vogal
        self.assertEqual(resposta['b'], 0) # confere se b não é uma vogal
    
    def test_sort(self):
        alfabeto = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
        
        dados = {"words": alfabeto ,"order": "desc"}
        request_test = requests.post("http://localhost:5000/sort", json=dados) # Realiza a request
        resposta = request_test.json() # pega os dados da resposta
        self.assertEqual(request_test.ok, True) # testa a request
        self.assertEqual(resposta, ["z","y","x","w","v","u","t","s","r","q","p","o","n","m","l","k","j","i","h","g","f","e","d","c","b","a"]) # confere se a lista esta organizada decrescente

        dados = {"words": alfabeto,"order":"asc"}
        request_test = requests.post("http://localhost:5000/sort", json=dados) # Realiza a request
        resposta = request_test.json() # pega os dados da resposta
        self.assertEqual(request_test.ok, True) # testa a request
        self.assertEqual(resposta, alfabeto) # confere se a lista esta organizada crescente
        
        